
3. Installation


3.1. Requirements and Dependencies

In order to run CometD applications, you need the Java Development Kit
(JDK) – version 7.0 or greater, and a compliant Servlet 3.0 or greater
Servlet Container such as Jetty.




The CometD implementation depends on few Jetty libraries, such as
jetty-util-ajax-<version>.jar and others.
These Jetty dependencies are typically packaged in the WEB-INF/lib
directory of your application .war file, and do not require you to
deploy your application .war file in Jetty: you CometD-based application
will work exactly in the same way in any other compliant Servlet 3.0 or
greater Servlet Container.




The current Jetty version CometD depends on is:





<jetty-version>9.2.14.v20151106</jetty-version>







3.2. Downloading and Installing

You can download the CometD distribution from http://download.cometd.org/.




Then unpack the distribution in a directory of your choice:





$ tar zxvf cometd-<version>-distribution.tgz
$ cd cometd-<version>/







3.3. Running the Demos

The CometD Demos contain:





Two full chat applications (one developed with Dojo, one with jQuery).


Examples of extensions such as message acknowledgement, reload, timesync and timestamp.


An example of how to echo private messages to a particular client only.


Clustered Auction demo (using the Oort clustering).





3.3.1. Running the Demos with Maven

This mode of running the CometD Demos is suggested if you want to take a quick
look at the CometD Demos and if you are prototyping/experimenting with your
application, but it’s not the recommended way to deploy a CometD application
in production. See the next section for the suggested
way to deploy your CometD application in production.




Maven requires you to set up the JAVA_HOME environment variable to point to
your JDK installation.




After that, running the CometD demos is very simple.
Assuming that $COMETD is the CometD installation directory, and that you have
the mvn executable in your path:





$ cd $COMETD
$ cd cometd-demo
$ mvn jetty:run





The last command starts an embedded Jetty that listens on port 8080.
Now point your browser to http://localhost:8080, to see the CometD Demos main page.








3.4. Deploying your CometD Application

When you develop a CometD application, you develop a standard Java EE Web Application
that is then packaged into a .war file.
You can follow the Primer section or the
CometD Tutorials for examples of how to build
and package your CometD application.




Once you have your CometD application packaged into a .war file, you can
deploy it to a any Servlet Container that supports Servlet 3.0 or greater.




Refer to this section for further
information and for specific instructions related to deployment on Servlet 3.0
(or greater) Containers.




3.4.1. Deploying your CometD Application in Jetty

The instructions below describe a very minimal Jetty setup that is needed
to run CometD applications.
Refer to the official Jetty documentation
for further details about configuring Jetty.




Follow these steps to deploy your CometD application into Jetty.
These instructions are valid for Unix/Linux operative systems,
but can be easily translated for the Windows operative system.




Download the Jetty distribution from the
Eclipse Jetty Downloads.




Then unpack the Jetty distribution in a directory of your choice,
for example /tmp:





$ cd /tmp
$ tar zxvf jetty-distribution-<version>.tar.gz





This creates a directory called /tmp/jetty-distribution-<version>/
that is referred to as the JETTY_HOME.




Create another directory of your choice, for example in your home
directory:





$ cd ~
$ mkdir jetty_cometd





This creates a directory called ~/jetty_cometd that is referred to as
the JETTY_BASE.




Since Jetty is a highly modular Servlet Container, the JETTY_BASE is
the directory where you configure Jetty with the Jetty modules that are
needed to run your CometD application.




In order to run CometD applications, Jetty needs to be configured with
these modules:





the http module, that provides support for the HTTP protocol


the websocket module, that provides support for the WebSocket protocol


the deploy module, that provides support for the deployment of .war
files





Therefore:





$ cd $JETTY_BASE
$ java -jar $JETTY_HOME/start.jar --add-to-start=http,websocket,deploy





Now Jetty is configured to run CometD applications, and you just need
to deploy your .war file to Jetty (you can use the CometD Demos .war
file if you have not built your application yet):





$ cp /path/to/cometd_application.war $JETTY_BASE/webapps/





Now you can start Jetty:





$ cd $JETTY_BASE
$ java -jar $JETTY_HOME/start.jar





This last command starts Jetty, which deploys your .war file and makes
your CometD application "live".






3.4.2. Running the Demos with Another Servlet Container

Steps similar to what described above for Jetty are what you need to do
to deploy your CometD application to different Servlet Containers.




Refer to the specific Servlet Container configuration manual for how to deploy
the CometD application .war file in the Servlet Container of your choice.










$ cd /tmp
$ mvn archetype:generate -DarchetypeCatalog=http://cometd.org
...
Choose archetype:
1: local -> org.cometd.archetypes:cometd-archetype-dojo-jetty9
2: local -> org.cometd.archetypes:cometd-archetype-spring-dojo-jetty9
3: local -> org.cometd.archetypes:cometd-archetype-jquery-jetty9
4: local -> org.cometd.archetypes:cometd-archetype-spring-jquery-jetty9
Choose a number:





As you can see, there are four archetypes available that build a skeleton application
using the Dojo or jQuery JavaScript toolkits, both with the choice of using
Jetty 9 and/or Spring.
Choose Dojo with Jetty 9, which is archetype number 1.
The archetype generation requires that you define several properties and generates
the application skeleton for you, for example:





Choose a number: : 1
Define value for property 'groupId': : org.cometd.primer
Define value for property 'artifactId': : dojo-jetty9-primer
Define value for property 'version':  1.0-SNAPSHOT: :
Define value for property 'package':  org.cometd.primer: :
[INFO] Using property: cometdVersion = 3.0.0
[INFO] Using property: jettyVersion = 9.2.0.v20140526
[INFO] Using property: slf4jVersion = 1.7.7
Confirm properties configuration:
groupId: org.cometd.primer
artifactId: dojo-jetty9-primer
version: 1.0-SNAPSHOT
package: org.cometd.primer
cometdVersion: 3.0.0
jettyVersion: 9.2.0.v20140526
slf4jVersion: 1.7.7
Y: :
...
[INFO] BUILD SUCCESS





Then:





$ cd dojo-jetty9-primer/






